#!/bin/sh
## No Header
## 05/05/2017 - 15:33:47

if [ $# -lt 1 ]
then
    echo "No commit message, no add and no push."
    exit
fi

message=$1
shift 1

files=$@
if [ $# -lt 1 ]
then
    files="--all"
fi

git add $files
git commit -m $message
git push
