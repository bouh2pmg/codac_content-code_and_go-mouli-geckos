#!/bin/bash

function err()
{
    >&2 echo "Error."
    exit 1
}

if [ $# -lt 1 ]; then
    err
fi

line=$(sed "/^$1:/!d" /etc/passwd)
if [ -z $line ]; then
   err
fi

home=$(echo $line | gawk -F: '{print $6}')
if [ -z $home ]; then
    err
fi
shell=$(echo $line | gawk -F: '{print $7}')
if [ -z $shell ]; then
    err
fi

echo "Home Directory: $home"
echo "Default Shell: $shell"
