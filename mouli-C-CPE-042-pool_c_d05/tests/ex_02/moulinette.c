#include <stdio.h>

char *my_revstr(char *);

int main(int argc, char const *argv[])
{
  char sigma[6] = "sigma";
  char alpha[6] = "alpha";
  char beta[5] = "beta";
  
  printf("%s\n", my_revstr(sigma));
  printf("%s\n", my_revstr(alpha));
  printf("%s\n", my_revstr(beta));
	(void)argc;
	(void)argv;
	return 0;
}
