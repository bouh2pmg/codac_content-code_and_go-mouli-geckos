#include <string.h>

char *my_revstr(char *str)
{
  int i = -1;
  char tmp;
  int len = strlen(str);
  int max = len / 2 - 1;

  while (++i <= max)
    {
      tmp = str[i];
      str[i] = str[len - i - 1];
      str[len - i - 1] = tmp;
    }
  return str;
}
