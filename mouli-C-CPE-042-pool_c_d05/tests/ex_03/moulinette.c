#include <stdio.h>

int my_strcmp(char *, char *);

int main(int argc, char const *argv[])
{
	char *str1 = "lol";
	char *str2 = "Coucou !";
	char *str3 = "Hmmm.. ok ! see you tomorrow";
	char str4[4] = "Tot";

	printf("%d\n", my_strcmp(str1, str2));
	printf("%d\n", my_strcmp(str1, str1));
	printf("%d\n", my_strcmp(str4, str1));
	printf("%d\n", my_strcmp(str3, str4));
	printf("%d\n", my_strcmp(str4, str3));
	(void)argc;
	(void)argv;
	return 0;
}
