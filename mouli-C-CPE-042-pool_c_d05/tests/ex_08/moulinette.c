#include <stdio.h>

int my_putnbr_base(int, char*);

int main(int argc, char const *argv[])
{
  my_putnbr_base(546654, "0123456789");
  my_putnbr_base(546654, "0123456789ABCDEF");
  my_putnbr_base(54, "01");
  if (my_putnbr_base(5, "!") != 5)
    printf("Bad return :(\n");
  my_putnbr_base(5465, "+-*/%()-_=");
  (void)argc;
  (void)argv;
  return 0;
}
