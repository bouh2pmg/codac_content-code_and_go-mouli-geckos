#include <stdio.h>

char *my_strcupcase(char *);

int main(int argc, char const *argv[])
{
	char str1[] = "Taritifico";
	char str2[] = "Apocalypticodramatique";
	char str3[] = "AAAAAAAAAAAAAAAAAAAAAAAAA";
	char str4[] = "zZzZzZzZzZzZ";
	char str5[] = "";

	my_strupcase(str1);
	my_strupcase(str2);
	my_strupcase(str3);
	my_strupcase(str4);
	my_strupcase(str5);
	(void)argc;
	(void)argv;
	return 0;
}
