char *my_strupcase(char *str)
{
  int i = 0;

  while (str && str[i])
    {
      if (str[i] >= 'a' && str[i] <= 'z')
	str[i] = str[i] - 'a' + 'A';
      ++i;
    }
  return str;
}
