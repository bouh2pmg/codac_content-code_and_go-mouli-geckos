#include <stdio.h>
#include <stdlib.h>

char *my_strstr(char *, char *);

int main(int argc, char const *argv[])
{
  char tab[7] = "abcdef";
  char total[27] = "abcdefghijklmnopqrstuvwxyz";
  char search[2] = "g";
  char gg[7] = "aGgaga";
  
  printf("%s\n", my_strstr(total, tab));
  printf("%s\n", my_strstr(gg, search));
  if (my_strstr(search, gg) == NULL)
    printf("Segmentation Fault\n");
  (void)argc;
  (void)argv;
  return 0;
}
