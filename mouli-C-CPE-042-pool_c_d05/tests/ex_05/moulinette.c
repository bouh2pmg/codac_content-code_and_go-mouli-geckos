#include <stdio.h>

int my_str_isalpha(char *);

int main(int argc, char const *argv[])
{
	char str1[] = "Taritifico";
	char str2[] = "Apocalyp_-_ticodramatique";
	char str3[] = "AAAAAAAAA666AAAAAAAAAAAAAAAA";
	char str4[] = "zZzZzZzZzZzZ()";
	char str5[] = "";

	printf("%d\n", my_str_isalpha(str1));
	printf("%d\n", my_str_isalpha(str2));
	printf("%d\n", my_str_isalpha(str3));
	printf("%d\n", my_str_isalpha(str4));
	printf("%d\n", my_str_isalpha(str5));
	(void)argc;
	(void)argv;
	return 0;
}
