int my_str_isalpha(char *str)
{
  int i = 0;

  if (!str || !str[i])
    return 0;
  while (str && str[i])
    {
      if (!((str[i] >= 'a' && str[i] <= 'z')
	    || (str[i] >= 'A' && str[i] <= 'Z')))
	return 0;
      ++i;
    }
  return 1;
}
