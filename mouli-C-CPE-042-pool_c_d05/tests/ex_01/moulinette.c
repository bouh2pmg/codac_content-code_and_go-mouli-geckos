#include <stdio.h>

int array_sum(int *, int);

int main(int argc, char const *argv[])
{
  int tab[4] = {3,4,5,6};
  int fake[2] = {1,2};
  int tab2[5] = {1,2,3,4,5};

  printf("%d\n", array_sum(tab, 4));
  printf("%d\n", array_sum(fake, -1));
  printf("%d\n", array_sum(tab2, 4)); // Yes it's 4 on purpose
  (void)argc;
  (void)argv;
  return 0;
}
