#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

void my_reset_ptr(char **);

int main(int argc, char const *argv[])
{
  char toto[] = "Salut je fonctionne !";
  int len = strlen(toto);
  char *titi = "Ou pas :)";
  int len_titi = strlen(titi);
  char *dest = malloc(sizeof(*toto) * (len + len_titi + 1));

  my_reset_ptr(&dest);
  if (dest != NULL)
    printf("Ptr is not null :(\n");
  else
    printf("Ok\n");
free(dest);
    return 0;
}


