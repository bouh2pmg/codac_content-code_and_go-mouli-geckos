#include <stdlib.h>
#include <stdio.h>

void my_show_str(char**);

int main(int argc, char const *argv[])
{
	char *tab[] = {
		"Hello",
		"Students",
		"ZogZog",
		"BOOUUUUM",
		"",
		NULL
	};

	my_show_str(tab);
	(void)argc;
	(void)argv;
	return 0;
}