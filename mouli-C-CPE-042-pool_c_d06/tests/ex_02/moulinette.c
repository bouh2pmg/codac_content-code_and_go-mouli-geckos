#include <stdio.h>

void my_show_address(int *);

int main(int argc, char const *argv[])
{
	int *i;
	int *j;
	int *k;

	i = (int *)1;
	j = (int *)423232;
	k = (int *)0;
	my_show_address(i);
	my_show_address(j);
	my_show_address(k);

	(void)argc;
	(void)argv;
	return 0;
}