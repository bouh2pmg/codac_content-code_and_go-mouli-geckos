#include <stdlib.h>
#include <stdio.h>

char *my_concat_str(char **);

int main(int argc, char const *argv[])
{
	char *tab1[] = {
		"zog",
		"zog",
		NULL
	};
	char *tab2[] = {
		"Bonjour",
		"",
		" ",
		"les amis ! ",
		NULL
	};

	printf("%s\n", my_concat_str(tab1));
	printf("%s\n", my_concat_str(tab2));
	(void)argc;
	(void)argv;
	return 0;
}