#include <stdlib.h>
#include <unistd.h>

char	*my_concat_str(char **str)
{
  int	i;
  int	j;
  int	counter;
  char	*concat;

  i = -1;
  counter = 0;
  while (str[++i])
    {
      j = -1;
      while (str[i][++j]);
      counter += j;
    }
  if ((concat = malloc(sizeof(*concat) * (counter + 1))) == NULL)
    return NULL;
  i = -1;
  counter = -1;
  while (str[++i])
    {
      j = -1;
      while (str[i][++j])
	concat[++counter] = str[i][j];
    }
  concat[++counter] = 0;
  return(concat);
}
