#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

char *my_strcpy(char *dst, const char *src);

int main(int argc, char const *argv[])
{
  char toto[] = "Salut je fonctionne !";
  int len = strlen(toto);
  char *titi = "Ou pas :)";
  int len_titi = strlen(titi);
  char *dest = malloc(sizeof(*toto) * (len + len_titi + 1));
  dest[len]= ' ';
  int i = 0;
  while (i < len)
    {
      dest[len + 1 + i] = titi[i];
      ++i;
    }
  dest[len + 1 + i] = '\0';
  char *ret = my_strcpy(dest, toto);
  
if (ret == dest)
  printf("Bonne valeur de retour\n");
 else
  printf("Mauvaise valeur de retour\n");
printf("%s\n", dest);
  return 0;
}


