#include <stdlib.h>

int	*my_up(int value)
{
  int	*ret;

  if ((ret = malloc(sizeof(*ret) * 2)) == NULL)
    return (NULL);

  ret[0] = value;
  ret[1] = value * 2;

  return(ret);
}
