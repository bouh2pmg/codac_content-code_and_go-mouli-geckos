#include <stdlib.h>
#include <stdio.h>

int *my_up(int);

int main(int argc, char const *argv[])
{
	int *ret1;
	int *ret2;
	int *ret3;

	ret1 = my_up(42);
	ret2 = my_up(0);
	ret3 = my_up(999);
	printf("%d %d\n", ret1[0], ret1[1]);
	printf("%d %d\n", ret2[0], ret2[1]);
	printf("%d %d\n", ret3[0], ret3[1]);
	free(ret1);
	free(ret2);
	free(ret3);
	(void)argc;
	(void)argv;
	return 0;
}