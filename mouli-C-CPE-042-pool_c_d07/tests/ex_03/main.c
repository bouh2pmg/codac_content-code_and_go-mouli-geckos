#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int	main(int ac, char **av)
{
  int	i;
  int	fd;

  if (ac < 3 || (fd = open(av[1], O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
    {
      printf("Error.\n");
      return (-1);
    }

  i = 1;
  while (++i < ac)
    {
      write(fd, av[i], strlen(av[i]));
      write(fd, "\n", 1);
    }

  close(fd);
  return (0);
}
