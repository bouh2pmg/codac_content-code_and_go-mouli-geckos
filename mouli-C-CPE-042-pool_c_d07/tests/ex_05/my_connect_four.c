#include "my_connect_four.h"

/*
** Initialization of read_buffer
*/

void initialization_read_buffer(char* read_buffer)
{
  int counter = 0;

  while (counter < 4096)
  {
    read_buffer = '\0';
    counter = counter + 1;
  }
}

/*
** Show game array
*/

void show_game(char **game_array, int player_number)
{
  int counter_y = 0;

  printf("| 1 | 2 | 3 | 4 | 5 | 6 | 7 |\n");
  printf("=============================\n");

  // Writing a game line
  while (counter_y < 6)
  {
    printf("| %c | %c | %c | %c | %c | %c | %c |\n", game_array[counter_y][0],
    game_array[counter_y][1], game_array[counter_y][2],
    game_array[counter_y][3], game_array[counter_y][4],
    game_array[counter_y][5], game_array[counter_y][6]);
    counter_y = counter_y + 1;
  }
}

/*
** Initialization of the game array
*/

char** init_game_array()
{
  char** array_to_ret = NULL;
  int counter_y = 0;
  int counter_x;

  // creation of the game array lines
  array_to_ret = malloc(sizeof(*array_to_ret) * 8);

  // creation of each line
  while (counter_y < 7)
  {
    counter_x = 0;
    array_to_ret[counter_y] = malloc(sizeof(*array_to_ret[counter_y]) * 8);
    while (counter_x < 7)
    {
      array_to_ret[counter_y][counter_x] = '-';
      counter_x = counter_x + 1;
    }
    array_to_ret[counter_y][counter_x] = '\0';
    counter_y = counter_y + 1;
  }
  array_to_ret[counter_y] = NULL;

  return (array_to_ret);
}

/*
** Get a specific case of game_array
*/

char get_case(char** game_array, int coord_y, int coord_x, char played_piece)
{
  if (coord_y < 0 || coord_y > 5 || coord_x < 0 || coord_x > 6)
    return ('-');
  else if (game_array[coord_y][coord_x] == played_piece)
    return ('+');
  return ('-');
}

/*
** Searches 4 '+' caracters one after another in a string of 7 of length
*/

int check_plus_4(char *str)
{
    int counter = 0;
    int counter_2;
    int tmp_counter;

    while (str[counter] != '\0')
    {
      tmp_counter = counter;
      if (str[counter] == '+')
      {
        counter_2 = counter;
        while (str[counter_2] == '+')
          counter_2 = counter_2 + 1;
        if (counter_2 - counter >= 4)
          return (1);
      }
      counter = tmp_counter + 1;
    }

    return (0);
}

/*
** Check if there is a victory
*/

int check_victory(char** game_array, int line, int column)
{
  char played_piece = game_array[line][column];

  // Creation des 4 grosses diagonales pour tester dans toutes les directions
  char diag_left[8] =
  {
    get_case(game_array, line - 3, column - 3, played_piece),   //   X
    get_case(game_array, line - 2, column - 2, played_piece),   //    X
    get_case(game_array, line - 1, column - 1, played_piece),   //     X
    get_case(game_array, line, column, played_piece),           //      X
    get_case(game_array, line + 1, column + 1, played_piece),   //       X
    get_case(game_array, line + 2, column + 2, played_piece),   //        X
    get_case(game_array, line + 3, column + 3, played_piece),   //         X
    '\0'
  };
  char diag_right[8] =
  {
    get_case(game_array, line - 3, column + 3, played_piece),   //         X
    get_case(game_array, line - 2, column + 2, played_piece),   //        X
    get_case(game_array, line - 1, column + 1, played_piece),   //       X
    get_case(game_array, line, column, played_piece),           //      X
    get_case(game_array, line + 1, column - 1, played_piece),   //     X
    get_case(game_array, line + 2, column - 2, played_piece),   //    X
    get_case(game_array, line + 3, column - 3, played_piece),   //   X
    '\0'
  };
  char vertical[8] =
  {
    get_case(game_array, line - 3, column, played_piece),       //      X
    get_case(game_array, line - 2, column, played_piece),       //      X
    get_case(game_array, line - 1, column, played_piece),       //      X
    get_case(game_array, line, column, played_piece),           //      X
    get_case(game_array, line + 1, column, played_piece),       //      X
    get_case(game_array, line + 2, column, played_piece),       //      X
    get_case(game_array, line + 3, column, played_piece),       //      X
    '\0'
  };
  char horizontal[8] =
  {
    get_case(game_array, line, column - 3, played_piece),       //
    get_case(game_array, line, column - 2, played_piece),       //
    get_case(game_array, line, column - 1, played_piece),       //
    get_case(game_array, line, column, played_piece),           //   XXXXXXX
    get_case(game_array, line, column + 1, played_piece),       //
    get_case(game_array, line, column + 2, played_piece),       //
    get_case(game_array, line, column + 3, played_piece),       //
    '\0'
  };

  // Check if one of great diagonals contains 4 caracters one after other
  if (check_plus_4(diag_left) == 1 ||
      check_plus_4(diag_right) == 1 ||
      check_plus_4(vertical) == 1 ||
      check_plus_4(horizontal) == 1)
      return (3);
  return (1);
}

/*
** Insertion of a new piece in the game array, proof of that the current player played
** and checking of victory
*/

int play_game(char* read_buffer, char** game_array, int player_number)
{
  int column = atoi(read_buffer);

  if (column < 1 || column > 7) // Return false if the entered number is not good
    return (0);
  else // Return false if there is no place on the selectionned column
    {
      int line = 5;
      while (line > -1 && game_array[line][column - 1] != '-')
        line = line - 1;
      if (line == -1)
        return (0);
      else
      {
        // Puting the piece in the array if there is a place and it's good
        if (player_number == 1)
          game_array[line][column - 1] = 'X';
        else if (player_number == 2)
          game_array[line][column - 1] = 'O';

        int result = check_victory(game_array, line, column - 1);

        // Check of the fact if the game is a draw
        if (result == 3)
          return (3);
        else
        {
          return (check_draw(game_array));
        }
      }
    }
  return (0);
}

/*
** Checks if a game is a draw with verifying if all the cases of array are filled
*/

int  check_draw(char **game_array)
{
    int counter_y = 0;
    int counter_x;

    while (counter_y < 6)
    {
      counter_x = 0;
      while (counter_x < 7)
      {
        if (game_array[counter_y][counter_x] == '-')
          return (1);
	counter_x++;
      }
      counter_y++;
    }
    return (2);
}

/*
** Launching of the game loop
*/

void launch_game()
{
  char** game_array = init_game_array();
  int continue_game = 1;
  int victory = -1;
  char* read_buffer;
  int player_number = 1;

  read_buffer = malloc(sizeof(*read_buffer) * 4096);

  while (continue_game == 1)
  {
    // Shows the game
    show_game(game_array, player_number);

    // Show the entry for the player to fill
    if (player_number == 1)
      printf("\nPlayer #1's turn !  ");
    else if (player_number == 2)
      printf("\nPlayer #2's turn !  ");

    // initializes the buffer for read
    initialization_read_buffer(read_buffer);
    int	t;
    int result;
  
    if ((t = gecko_read(read_buffer)) == 0)
      result = 2;
    if (t > 0)
      {
	printf("%s\n", read_buffer);
	read_buffer[t - 1] = 0;

	// places the pieces on the game
	result = play_game(read_buffer, game_array, player_number);
      }
    if (result == 1)  // The piece was placed correctly
      {
        if (player_number == 1)
          player_number = 2;
        else if (player_number == 2)
          player_number = 1;
      }
    else if (result == 2) // There is a draw
    {
      printf("It's a draw !\n");
      continue_game = 0;
    }
    else if (result == 3) // There is a current player victory
    {
      show_game(game_array, player_number);
      printf("\nCongratulations Player #%d, you win !\n", player_number);
      continue_game = 0;
    }
  }
}

/*
** Lancement du programme
*/

int main(int ac, char **av)
{
  launch_game();
}
