#ifndef     __MY_CONNECT_FOUR__
# define    __MY_CONNECT_FOUR__

# include   <stdio.h>
# include   <stdlib.h>
# include   <unistd.h>

void        my_putchar(char c);
void        my_putstr(char *str);

//          Initializations
void        initialization_read_buffer(char* read_buffer);
char**      init_game_array();

//          Showings
void        show_game(char **game_array, int player_number);

//          Game functions
char        get_case(char** game_array, int coord_y, int coord_x, char played_piece);
int         play_game(char* read_buffer, char** game_array, int player_number);

//          Checking functions
int         check_plus_4(char *str);
int         check_victory(char** game_array, int line, int column);
int         check_draw(char** game_array);

//          Main functions
void        launch_game();
int         main(int ac, char **av);

#endif      /* __MY_CONNECT_FOUR__ */
