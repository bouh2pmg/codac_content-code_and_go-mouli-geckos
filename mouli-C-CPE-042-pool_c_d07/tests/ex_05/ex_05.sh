#!/bin/sh
## No Header
## 03/05/2017 - 16:05:24

binary=$1

#column victory
./$binary <<EOF
1
2
1
2
1
2
0
9
0
9
1
EOF

#diag victory
./$binary <<EOF
1
2
2
3
1
3
3
4
4
4
4
EOF

#Other diag victory
./$binary <<EOF
1
1
1
1
2
2
3
2
1
4
1
3
EOF

#Draw, full board
./$binary <<EOF
1
1
2
2
3
3
5
5
6
6
7
7
1
1
2
2
3
3
5
5
6
6
7
7
1
1
2
2
3
3
5
5
6
6
7
4
4
4
4
4
4
7
4
5
6
7
8
9
EOF

#Line
./$binary <<EOF
7
1
1
2
2
3
3
4
4
EOF
