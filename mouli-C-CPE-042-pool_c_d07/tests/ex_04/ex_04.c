#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

char	*my_read_file(char *file)
{
  int	fd;
  char	buffer[4096];
  int	i;
  char	*ret;
  int	rd;

  if ((fd = open(file, O_RDONLY)) == -1)
    return (NULL);
  i = 0;
  while ((rd = read(fd, buffer, sizeof(buffer))) > 0)
    i += rd;
  if ((ret = malloc(sizeof(*ret) * (i + 1))) == NULL)
    return (NULL);
  if (lseek(fd, 0, SEEK_SET) < 0)
    return (NULL);
  i = 0;
  while ((rd = read(fd, buffer, sizeof(buffer))) > 0)
    {
      strncpy(&(ret[i]), buffer, rd);
      i += rd;
    }
  ret[i] = 0;
  close(fd);
  return (ret);
}

int	main(int argc, char **argv)
{
  char	*s;

  if (argc < 1)
    return (printf("Error.\n"));
  s = my_read_file(argv[1]);
  if (s != NULL)
    printf("%s", s);
  else
    printf("Error.\n");    
}
