#!/bin/bash

if [ $# -lt 1 ]
then
    echo -e "[ERREUR] $0 : pas assez d'arguments"
    echo -e "USAGE : $0 exercice [-c]"
    exit 2
fi

exercise=ex_$1
binary=$exercise.sh
output="output.test"

if [ $exercise != "ex_00" ]
then
    if [ ! -e $exercise ]
    then
	echo "KO: exercise [$binary] not found"
	exit 2
    fi
fi

if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
then
    cd ./tests && ./$binary ./$exercise &> "./${exercise}_REF"
else
    cp tests/$binary .
    sudo -u student ./$binary ./$exercise &> output.test
    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))

    if [ $? -ne 0 ]; then
	echo "KO: check your traces below this line..."
	echo "$res"
    else
	echo "OK"
    fi
fi
