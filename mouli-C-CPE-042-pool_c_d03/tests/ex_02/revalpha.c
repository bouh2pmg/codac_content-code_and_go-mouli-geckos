#include <unistd.h>

void revalpha()
{
  char c = 'Z' + 1;

  while (--c >= 'A')
    write(1, &c, 1);
  write(1, "\n", 1);
}
