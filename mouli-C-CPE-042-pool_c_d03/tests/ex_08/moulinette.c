#include <stdio.h>

int my_aff_combn(int n);

int main(int argc, char const *argv[])
{
  my_aff_combn(0);
  my_aff_combn(1);
  my_aff_combn(3);
  my_aff_combn(7);
  my_aff_combn(10);
  my_aff_combn(11);
  
  (void)argc;
  (void)argv;
  return 0;
}
