void print_unique(int *tab, int size);

int	main()
{
  int tab[] = {0};
  int tab2[] = {0, 1, 2, 0};
  int tab3[] = {0, 1, 2, 3, 0, 74, 0, 0, 2, 2};
  int tab4[] = {2, 3};

  print_unique(tab, 1);
  print_unique(tab2, 4);
  print_unique(tab3, 10);
  print_unique(tab4, 2);
  return 0;
}
