#include <stdio.h>
#include <stdlib.h>

int	follow_the_white_rabbit();

int	main()
{
  int i = 0;
  while (i < 13)
    {
      srandom(i);
      printf("Dice = %d\n-----\n", follow_the_white_rabbit());
      ++i;
    }
  return 0;
}
