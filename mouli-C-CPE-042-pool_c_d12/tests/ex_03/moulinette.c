#include <stdio.h>

int	magic_square(int *sqr);

int	main()
{
  int tab[] = {0, 0, 0, 0, 0, 0, 0, 0, 0}; // True => ret 0
  int tab2[] = {0, 1, 2, 0, 1, 2, 0, 1, 2}; // False => ret 1
  int tab3[] = {8, 1, 6, 3, 5, 7, 4, 9, 2}; // True => ret 0

  printf("%d %d %d\n", magic_square(tab), magic_square(tab2), magic_square(tab3));
  return 0;
}
