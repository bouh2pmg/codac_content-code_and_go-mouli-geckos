#include <stdio.h>
#include <stdlib.h>
int	**split_array(int *arr, int size, int *new_size1, int *new_size2);

int	main()
{
  int tab[] = {0};
  int tab2[] = {0, 1, 2, 0};
  int tab3[] = {0, 1, 2, 3, 0, 74, 0, 0, 2, 2};
  int tab4[] = {2, 3};

  int size1, size2, i = 0, j = 0;
  int	**ret = split_array(tab, 1, &size1, &size2);
  printf("New size => %d %d\n", size1, size2);
  while (i < size1)
    printf("%d ", ret[0][i++]);
  printf("\n");
  while (j < size2)
    printf("%d ", ret[1][j++]);
  printf("\n");
  i = 0;
  j = 0;
  free(ret[0]);
  free(ret[1]);
  free(ret);
  ret = split_array(tab3, 10, &size1, &size2);
  printf("New size => %d %d\n", size1, size2);
  while (i < size1)
    printf("%d ", ret[0][i++]);
  printf("\n");
  while (j < size2)
    printf("%d ", ret[1][j++]);
  printf("\n");
  free(ret[0]);
  free(ret[1]);
  free(ret);
  return (0);
}
