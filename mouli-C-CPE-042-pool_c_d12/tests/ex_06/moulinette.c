#include <stdio.h>

int	matrices_addition(int **, int column_offset, int row_offset, int size, int direction);

int	main()
{
  int	mat[8][8] =
    {
      {0, 1, 2, 3, 4, 24, 75, 32},
      {5, 6, 7, 8, 9, 23, 91, 42},
      {10, 11, 12, 13, 14, 42, 94, 123},
      {15, 16, 17, 18, 19, 11, 56, 93},
      {20, 21, 22, 23, 24, 29, 230, 299},
      {20, 21, 22, 23, 24, 29, 230, 299},
      {20, 21, 22, 23, 24, 29, 230, 299},
      {10, 11, 12, 13, 14, 42, 94, 123}
    };
  int *tab[8];

  tab[0] = mat[0];
  tab[1] = mat[1];
  tab[2] = mat[2];
  tab[3] = mat[3];
  tab[4] = mat[4];
  tab[5] = mat[5];
  tab[6] = mat[6];
  tab[7] = mat[7];
  printf("%d\n", matrices_addition(tab, 0, 0, 5, 3));
  printf("%d\n", matrices_addition(tab, 0, 0, 5, 6));
  printf("%d\n", matrices_addition(tab, 0, 0, 5, 0));
  printf("%d\n", matrices_addition(tab, 3, 3, 5, 11));
  printf("%d\n", matrices_addition(tab, 3, 3, 5, -2));
  printf("%d\n", matrices_addition(tab, 3, 22, 5, 2));
  
  printf("%d\n", matrices_addition(tab, 3, 4, 8, 9));
  printf("%d\n", matrices_addition(tab, 7, 5, 8, 6));
  printf("%d\n", matrices_addition(tab, 2, 6, 8, 7));
  return 0;
}
