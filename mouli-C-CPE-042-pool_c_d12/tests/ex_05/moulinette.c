#include <stdio.h>
#include <stdlib.h>

int **pascalTr(int);

static void print_pascal_tr_(int **tab, int size)
{
  int	i = 0;
  int	j;
  
  while (i < size)
    {
      j = 0;
      while (j < (i + 1))
	{
	  if (j < i)
	    printf("%d ", tab[i][j]);
	  else
	    printf("%d\n", tab[i][j]);
	  ++j;
	}
      ++i;
    }
  i = 0;
  while (i < size)
    free(tab[i++]);
  free(tab);
  printf("-----\n");
}

int	main()
{
  int	i;

  i = 0;
  while (i < 10)
    {
      print_pascal_tr_(pascalTr(i), i);
      ++i;
    }
  return (0);
}
