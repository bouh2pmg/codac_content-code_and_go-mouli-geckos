SELECT title as "Movie title", DATEDIFF(CURDATE(), release_date) as "Number of days passed" FROM movies HAVING `Number of days passed` IS NOT NULL;
