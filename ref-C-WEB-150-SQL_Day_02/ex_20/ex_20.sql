SELECT *
FROM movies
INTO OUTFILE '/tmp/movies.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ','
LINES TERMINATED BY '\n';
