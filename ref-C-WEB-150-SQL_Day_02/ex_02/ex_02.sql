SELECT zipcode AS "Zip codes" FROM profiles GROUP BY zipcode HAVING COUNT("zip codes") > 1 ORDER BY zipcode ASC;
