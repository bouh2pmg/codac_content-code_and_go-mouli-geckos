SELECT floor as "Floor number", SUM(seats) as "Total number of seats", COUNT(*) as "Total number of rooms" FROM rooms GROUP BY floor ORDER BY SUM(seats);
