#include <stdlib.h>
#include <stdio.h>
#include "struct.h"
#include "struct.h"
#include "abs.h"
#include "abs.h"

int main(int argc, char const *argv[])
{
	struct s_my_struct s;

	s.id = 42;
	s.str = "hello !";
	my_init(&s, 99, "Edited !");
	printf("%d %s\n", s.id, s.str);
	my_init(&s, 42, "Thinking about... nothing!");
	printf("%d %s\n", s.id, s.str);
	my_init(&s, 0, "");
	printf("%d %s\n", s.id, s.str);
	//	my_init(NULL, 0, NULL);
	(void)argc;
	(void)argv;
	return 0;
}
