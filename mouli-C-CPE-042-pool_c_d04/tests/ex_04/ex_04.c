void	my_swap(int *a, int *b)
{
  int	tmp;

  tmp = *a;
  *a = *b;
  *b = tmp;
}

void	my_sort_int_tab(int *tab, int size)
{
  int	i;
  int	j;

  i = 0;
  while (i < size)
    {
      j = 0;
      while (j < size)
	{
	  if (tab[i] < tab[j])
	    {
	      my_swap(&tab[i], &tab[j]);
	    }
	  ++j;
	}
      ++i;
    }
}
