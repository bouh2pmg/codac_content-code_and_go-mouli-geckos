#include <stdio.h>

void my_show_array(char **tab, int size);

int main()
{
  char tab[3][15]=
    {
      "not prepared !",
      "You",
      "are"
    };

  char tab2[5][5]=
    {
      "ab",
      "ba",
      "ca",
      "Da",
      "aE"
    };

  char test[5][5]=
    {
      "abc",
      "cba",
      "aBc",
      "bac",
      "cab"
    };

  char *f[3] = {tab[0], tab[1], tab[2]};

  char *f2[5] = {tab2[0], tab2[1], tab2[2], tab2[3], tab2[4]};

  char *f3[5] = {test[0], test[1], test[2], test[3], test[4]};

  my_show_array(f, 3);
  my_show_array(f2, 5);
  my_show_array(f3, 5);
  return 0;
}
