// No Header
// 28/04/2017 - 12:00:05

#include <stdio.h>

int	main()
{
  int	i;
  int	j;
  int	k;
  int	l;
  int	m;

  i = 987;
  j = 24;
  k = 42;
  l = 13;
  m = 666;
  my_swap(&i, &j);
  my_swap(&i, &k);
  my_swap(&j, &l);
  my_swap(&l, &k);
  my_swap(&k, &m);
  my_swap(&m, &j);
  printf("%d Mwahahaha %d\n%d%d %d\n", k, m, i, l, j);
}
